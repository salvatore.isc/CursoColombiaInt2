//
//  CursoColombiaInt2App.swift
//  CursoColombiaInt2
//
//  Created by Salvador Lopez on 07/06/23.
//

import SwiftUI

@main
struct CursoColombiaInt2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
